/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
	spaceRocks.clear();
	spaceRocks.shrink_to_fit(); 
	for (auto rocks : spaceRocks)
		delete rocks;
}
//---------------------------------------------------------------------------


void TutorialApplication::createScene(void)
{
	const float DAYS_IN_YEAR = 365.25;

	Planet *sun = new Planet(*mSceneMgr, 40, ColourValue(1, 1, 0.4));
	sun->secToCompleteRotation(8);
	spaceRocks.push_back(sun);

	Planet *mercury = new Planet(*mSceneMgr, 4.8, ColourValue(0.451, 0.45, 0.45));
	mercury->secToCompleteRotation(4);
	mercury->revolveAround(sun , 88, 60);
	spaceRocks.push_back(mercury);

	Planet *venus = new Planet(*mSceneMgr, 12.1, ColourValue(1, 0.6, 0));
	venus->secToCompleteRotation(4);
	venus->revolveAround(sun, 224.7, 80);
	spaceRocks.push_back(venus);

	Planet *earth = new Planet(*mSceneMgr, 12.7, ColourValue(0.4, 0.6, 1));
	earth->secToCompleteRotation(7);
	earth->revolveAround(sun, 365.2, 120);
	spaceRocks.push_back(earth);

	Planet *moon = new Planet(*mSceneMgr, 1.5, ColourValue(0.702, 0.702, 0.702));
	moon->secToCompleteRotation(5);
	moon->revolveAround(earth, 5, 15); // 1
	spaceRocks.push_back(moon);

	Planet *mars = new Planet(*mSceneMgr, 6.7, ColourValue(0.902, 0.451, 0));
	mars->secToCompleteRotation(7);
	mars->revolveAround(sun, 687, 170);
	spaceRocks.push_back(mars);

	Planet *jupiter = new Planet(*mSceneMgr, 35.8, ColourValue(1, 0.6, 0.2));
	jupiter->secToCompleteRotation(7);
	jupiter->revolveAround(sun, 11.86 * DAYS_IN_YEAR, 300);
	spaceRocks.push_back(jupiter);

	Planet *saturn = new Planet(*mSceneMgr, 31.6, ColourValue(1, 1, 0.702));
	saturn->secToCompleteRotation(7);
	saturn->revolveAround(sun, 29.46 * DAYS_IN_YEAR, 450);
	spaceRocks.push_back(saturn);

	Planet *uranus = new Planet(*mSceneMgr, 25.8, ColourValue(0.8, 1, 1));
	uranus->secToCompleteRotation(7);
	uranus->revolveAround(sun, 84 * DAYS_IN_YEAR, 550);
	spaceRocks.push_back(uranus);

	Planet *neptune = new Planet(*mSceneMgr, 23.5, ColourValue(0.2, 0.4, 1));
	neptune->secToCompleteRotation(7);
	neptune->revolveAround(sun, 165 * DAYS_IN_YEAR, 650);
	spaceRocks.push_back(neptune);

	Planet *pluto = new Planet(*mSceneMgr, 2.2, ColourValue(1, 1, 0.902));
	pluto->secToCompleteRotation(7);
	pluto->revolveAround(sun, 248 * DAYS_IN_YEAR, 750);
	spaceRocks.push_back(pluto);
}

//UPDATE
bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	for (auto i : spaceRocks) i->update(evt);

	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
