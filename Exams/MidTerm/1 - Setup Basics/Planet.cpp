#include "Planet.h"



Planet::Planet(SceneManager & sm, float size, ColourValue colour)
{
	ManualObject *cube = sm.createManualObject();
	cube->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_LIST);
	cube->colour(colour);
	cube->position(-(size / 2), -(size / 2), (size / 2)); //Front
	cube->position((size / 2), -(size / 2), (size / 2));
	cube->position(-(size / 2), (size / 2), (size / 2));
	cube->position((size / 2), (size / 2), (size / 2));
	cube->position(-(size / 2), -(size / 2), -(size / 2)); //Back
	cube->position((size / 2), -(size / 2), -(size / 2));
	cube->position(-(size / 2), (size / 2), -(size / 2));
	cube->position((size / 2), (size / 2), -(size / 2));
	cube->index(0); cube->index(3); cube->index(2); //Front Face
	cube->index(0); cube->index(1); cube->index(3);
	cube->index(1); cube->index(7); cube->index(3); //Right Face
	cube->index(1); cube->index(5); cube->index(7);
	cube->index(2); cube->index(7); cube->index(6); //Top Face
	cube->index(2); cube->index(3); cube->index(7);	
	cube->index(4); cube->index(1); cube->index(0); //Bottom Face
	cube->index(4); cube->index(5); cube->index(1);
	cube->index(4); cube->index(2); cube->index(6); //Left Face
	cube->index(4); cube->index(0); cube->index(2);
	cube->index(6); cube->index(7); cube->index(5); //Back Face
	cube->index(6); cube->index(5); cube->index(4);
	cube->end();

	m_Node = sm.getRootSceneNode()->createChildSceneNode();
	m_Node->attachObject(cube);
}

Planet::~Planet()
{
	if (m_Node != NULL) delete m_Node;
}

SceneNode * Planet::getNode()
{
	return m_Node;
}

void Planet::revolveAround(Planet *planet, float days, float distance)
{
	revSpeed = (DAYS_TO_FULL_REVOLUTION / days) * (360 / SECONDS_TO_FULL_REVOLUTION);
	m_NodeToRevolve = planet->getNode();
	m_Node->setPosition(m_NodeToRevolve->getPosition().x + distance, 0, 0);
}

void Planet::revolve(float degPerSecond)
{
	//Move to center
	if (m_NodeToRevolve != NULL) {
		Vector3 relPos = m_Node->getPosition() - m_NodeToRevolve->getPosition();
		m_Node->setPosition(relPos);
	}

	Radian rad = Radian(Degree(degPerSecond));
	Vector3 newPos;
	newPos.x = (m_Node->getPosition().x * Math::Cos(rad))
		+ (m_Node->getPosition().z * Math::Sin(rad));
	newPos.y = 0;
	newPos.z = (m_Node->getPosition().x * -Math::Sin(rad))
		+ (m_Node->getPosition().z * Math::Cos(rad));

	//Move back
	if (m_NodeToRevolve != NULL) 
		newPos += m_NodeToRevolve->getPosition();
	
	m_Node->setPosition(newPos);
}

void Planet::secToCompleteRotation(float secs)
{
	rotSpeed = 360 / secs;
}

void Planet::update(const FrameEvent & evt)
{
	if (rotSpeed != 0) {
		Vector3 yAxis = Vector3(0, 1, 0);
		m_Node->rotate(yAxis, Radian(Degree(rotSpeed * evt.timeSinceLastFrame)));
	}

	if (revSpeed != 0) 
		revolve(revSpeed * evt.timeSinceLastFrame);
}
