#pragma once
#include "BaseApplication.h"
#include <OgreManualObject.h>
#include <vector>

using namespace Ogre;
class Planet
{
public:
	//Instantiation will set everything instead
	Planet(SceneManager &sm, float size, MaterialPtr materialName);
	~Planet();

	void revolveAround(Planet *planet, float days, float distance = 0);
	void secToCompleteRotation(float secs);
	void update(const FrameEvent & evt);

private:
	const float SECONDS_TO_FULL_REVOLUTION = 60;
	const float DAYS_TO_FULL_REVOLUTION = 365.2;

	SceneNode *m_Node = NULL, *m_NodeToRevolve = NULL;
	float revSpeed = 0, rotSpeed = 0;

	SceneNode *getNode();
	void revolve(float degPerSecond);

	void generatePoints(ManualObject *obj, float radius, int quality = 20);
	Vector3 generatePolygonNormal(Vector3 &v0, Vector3 &v1, Vector3 &v2);
	void generateSphere(ManualObject *obj);

	std::vector<Vector3> m_VectorPoints;
};