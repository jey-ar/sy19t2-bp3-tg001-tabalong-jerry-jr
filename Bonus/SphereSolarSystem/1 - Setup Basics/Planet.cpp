#include "Planet.h"


Planet::Planet(SceneManager & sm, float size, MaterialPtr materialName)
{
	ManualObject *cube = sm.createManualObject();

	cube->begin(materialName->getName(), Ogre::RenderOperation::OT_TRIANGLE_LIST);

	//Use color if material is not using light
	if (!materialName->getTechnique(0)->getPass(0)->getLightingEnabled()) {
		cube->colour(materialName->getTechnique(0)->getPass(0)->getDiffuse());
	}
	Vector3 v0 = Vector3(-(size / 2), (size / 2), (size / 2));
	Vector3 v1 = Vector3(-(size / 2), -(size / 2), (size / 2));
	Vector3 v2 = Vector3( (size / 2), -(size / 2), (size / 2));
	Vector3 normal = generatePolygonNormal(v0, v1, v2);

	//Generate all points the sphere can have
	generatePoints(cube, size/2);
	//Generate the sphere using the points

	cube->position(-(size / 2), -(size / 2), (size / 2)); //Front
	cube->normal(normal);
	cube->position((size / 2), -(size / 2), (size / 2));
	cube->normal(normal);
	cube->position(-(size / 2), (size / 2), (size / 2));
	cube->normal(normal);
	cube->position((size / 2), (size / 2), (size / 2));
	cube->normal(normal);


	cube->position(-(size / 2), -(size / 2), -(size / 2)); //Back
	cube->normal(0, 0, -1);
	cube->position((size / 2), -(size / 2), -(size / 2));
	cube->normal(0, 0, -1);
	cube->position(-(size / 2), (size / 2), -(size / 2));
	cube->normal(0, 0, -1);
	cube->position((size / 2), (size / 2), -(size / 2));
	cube->normal(0, 0, -1);
	cube->position(-(size / 2), (size / 2), -(size / 2)); //Left 
	cube->normal(-1, 0, 0);
	cube->position(-(size / 2), -(size / 2), -(size / 2)); 
	cube->normal(-1, 0, 0);
	cube->position(-(size / 2), (size / 2), (size / 2));
	cube->normal(-1, 0, 0);
	cube->position(-(size / 2), -(size / 2), (size / 2));
	cube->normal(-1, 0, 0);
	cube->position((size / 2), (size / 2), -(size / 2)); //Right
	cube->normal(1, 0, 0);
	cube->position((size / 2), -(size / 2), -(size / 2));
	cube->normal(1, 0, 0);
	cube->position((size / 2), (size / 2), (size / 2));
	cube->normal(1, 0, 0);
	cube->position((size / 2), -(size / 2), (size / 2));
	cube->normal(1, 0, 0);

	cube->position(-(size / 2), (size / 2), -(size / 2)); //Top //16
	cube->normal(0, 1, 0);
	cube->position((size / 2), (size / 2), -(size / 2));
	cube->normal(0, 1, 0);
	cube->position(-(size / 2), (size / 2), (size / 2));
	cube->normal(0, 1, 0);
	cube->position((size / 2), (size / 2), (size / 2));
	cube->normal(0, 1, 0);
	cube->position(-(size / 2), -(size / 2), -(size / 2)); //Bottom //20
	cube->normal(0, -1, 0);
	cube->position((size / 2), -(size / 2), -(size / 2));
	cube->normal(0, -1, 0);
	cube->position(-(size / 2), -(size / 2), (size / 2));
	cube->normal(0, -1, 0);
	cube->position((size / 2), -(size / 2), (size / 2));
	cube->normal(0, -1, 0);

	cube->index(0); cube->index(3); cube->index(2); //Front Face
	cube->index(0); cube->index(1); cube->index(3);
	cube->index(6); cube->index(7); cube->index(5); //Back Face
	cube->index(6); cube->index(5); cube->index(4);
	cube->index(9); cube->index(10); cube->index(8); //Left Face
	cube->index(9); cube->index(11); cube->index(10);
	cube->index(15); cube->index(12); cube->index(14); //Right Face
	cube->index(15); cube->index(13); cube->index(12);
	cube->index(18); cube->index(17); cube->index(16); //Top Face
	cube->index(18); cube->index(19); cube->index(17);	
	cube->index(20); cube->index(23); cube->index(22); //Bottom Face
	cube->index(20); cube->index(21); cube->index(23);

	cube->end();

	m_Node = sm.getRootSceneNode()->createChildSceneNode();
	m_Node->attachObject(cube);
}

Planet::~Planet()
{
	if (m_Node != NULL) delete m_Node;
}

SceneNode * Planet::getNode()
{
	return m_Node;
}

void Planet::revolveAround(Planet *planet, float days, float distance)
{
	revSpeed = (DAYS_TO_FULL_REVOLUTION / days) * (360 / SECONDS_TO_FULL_REVOLUTION);
	m_NodeToRevolve = planet->getNode();
	m_Node->setPosition(m_NodeToRevolve->getPosition().x + distance, 0, 0);
}

void Planet::revolve(float degPerSecond)
{
	//Move to center
	if (m_NodeToRevolve != NULL) {
		Vector3 relPos = m_Node->getPosition() - m_NodeToRevolve->getPosition();
		m_Node->setPosition(relPos);
	}

	Radian rad = Radian(Degree(degPerSecond));
	Vector3 newPos;
	newPos.x = (m_Node->getPosition().x * Math::Cos(rad))
		+ (m_Node->getPosition().z * Math::Sin(rad));
	newPos.y = 0;
	newPos.z = (m_Node->getPosition().x * -Math::Sin(rad))
		+ (m_Node->getPosition().z * Math::Cos(rad));

	//Move back
	if (m_NodeToRevolve != NULL) 
		newPos += m_NodeToRevolve->getPosition();
	
	m_Node->setPosition(newPos);
}

void Planet::generatePoints(ManualObject *obj, float rad, int quality)
{
	float M_PI = 3.14159;
	for (float hor = 0; hor < 2 * M_PI; hor += M_PI / quality)
	{
		for (float vert = 0; vert < M_PI; vert += M_PI / quality)
		{
			Vector3 point;
			//point.x = rad * Math::Cos(hor) * sin(vert);
			//point.y = rad * sin(hor) * sin(vert);
			//point.z = rad * cos(vert);
			point.x = rad * Math::Cos(vert);
			point.y = rad * Math::Cos(hor) * Math::Sin(vert);
			point.z = rad * Math::Sin(hor) * Math::Sin(vert);


			m_VectorPoints.push_back(point);
		}
	}
	return;
}

Vector3 Planet::generatePolygonNormal(Vector3 &v0, Vector3 &v1, Vector3 &v2)
{
	Vector3 edge1 = v1 - v0;
	Vector3 edge2 = v2 - v0;
	Vector3 normal = edge1.crossProduct(edge2);
	normal.normalise();
	return normal;
}

void Planet::generateSphere(ManualObject * obj)
{
	int quality = 10;

	for (int i = 0; i < quality; i++) {

	}
}

void Planet::secToCompleteRotation(float secs)
{
	rotSpeed = 360 / secs;
}

void Planet::update(const FrameEvent & evt)
{
	if (rotSpeed != 0) {
		Vector3 yAxis = Vector3(0, 1, 0);
		m_Node->rotate(yAxis, Radian(Degree(rotSpeed * evt.timeSinceLastFrame)));
	}

	if (revSpeed != 0) 
		revolve(revSpeed * evt.timeSinceLastFrame);
}