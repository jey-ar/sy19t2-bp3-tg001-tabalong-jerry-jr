/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}
//---------------------------------------------------------------------------

ManualObject* TutorialApplication::createCube(float size = 1, ColourValue color = ColourValue::White) {

	ManualObject *cube = mSceneMgr->createManualObject();

	cube->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_LIST);

	cube->colour(color);

	//Front
	cube->position(-(size / 2), -(size / 2), (size / 2)); //BL 0
	cube->position((size / 2), -(size / 2), (size / 2)); //BR 1
	cube->position(-(size / 2), (size / 2), (size / 2)); //TL 2
	cube->position((size / 2), (size / 2), (size / 2)); //TR 3

	cube->colour(ColourValue::Red);
	//Back
	cube->position(-(size / 2), -(size / 2), -(size / 2)); //BL 4
	cube->position((size / 2), -(size / 2), -(size / 2)); //BR 5
	cube->position(-(size / 2), (size / 2), -(size / 2)); //TL 6
	cube->position((size / 2), (size / 2), -(size / 2)); //TR 7

	//Front Face
	cube->index(0); cube->index(3); cube->index(2);
	cube->index(0); cube->index(1); cube->index(3);

	//Right Face
	cube->index(1); cube->index(7); cube->index(3);
	cube->index(1); cube->index(5); cube->index(7);

	//Top Face
	cube->index(2); cube->index(7); cube->index(6);
	cube->index(2); cube->index(3); cube->index(7);

	//Bottom Face
	cube->index(4); cube->index(1); cube->index(0);
	cube->index(4); cube->index(5); cube->index(1);

	//Left Face
	cube->index(4); cube->index(2); cube->index(6);
	cube->index(4); cube->index(0); cube->index(2);

	//Back Face
	cube->index(6); cube->index(7); cube->index(5);
	cube->index(6); cube->index(5); cube->index(4);

	cube->end();
	return cube;
}

void TutorialApplication::createScene(void)
{
    // Create your scene here :)
	ManualObject *cube = createCube(10, ColourValue::Blue);
	ManualObject *center = createCube(5, ColourValue::Red);

	cubeNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	cubeNode->attachObject(cube);

	SceneNode * centerNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	centerNode->attachObject(center);

	cubeNode->setPosition(30, 0, 0);
	
}
float deg = 0;
//UPDATE
bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	
	Radian rad = Radian(Degree(deg));

	//Vector3 oldPos = cubeNode->getPosition();
	cubeNode->setPosition((cubeNode->getPosition().x * Math::Cos(rad)) + (cubeNode->getPosition().z * Math::Sin(rad)), 
		0, 
		(cubeNode->getPosition().x * -Math::Sin(rad)) + (cubeNode->getPosition().z * Math::Cos(rad)));
	
	deg = 60 * evt.timeSinceLastFrame;

	
	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
