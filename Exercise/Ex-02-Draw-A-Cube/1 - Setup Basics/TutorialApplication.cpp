/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}
//---------------------------------------------------------------------------

ManualObject* TutorialApplication::createCube(float xPos = 0, float yPos = 0, float zPos = 0, float size = 1, ColourValue color = ColourValue::White) {

	ManualObject *triangle = mSceneMgr->createManualObject();
	triangle->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_LIST);

	triangle->colour(color);

	//Front
	triangle->position(xPos, yPos, zPos); //BL 0
	triangle->position(xPos + size, yPos, zPos); //BR 1
	triangle->position(xPos, yPos + size, zPos); //TL 2
	triangle->position(xPos + size, yPos + size, zPos); //TR 3

	//Back
	triangle->position(xPos, yPos, zPos - size); //BL 4
	triangle->position(xPos + size, yPos, zPos - size); //BR 5
	triangle->position(xPos, yPos + size, zPos - size); //TL 6
	triangle->position(xPos + size, yPos + size, zPos - size); //TR 7

	//Front Face
	triangle->index(0); triangle->index(3); triangle->index(2);
	triangle->index(0); triangle->index(1); triangle->index(3);

	//Right Face
	triangle->index(1); triangle->index(7); triangle->index(3);
	triangle->index(1); triangle->index(5); triangle->index(7);

	//Top Face
	triangle->index(2); triangle->index(7); triangle->index(6);
	triangle->index(2); triangle->index(3); triangle->index(7);

	//Bottom Face
	triangle->index(4); triangle->index(1); triangle->index(0);
	triangle->index(4); triangle->index(5); triangle->index(1);

	//Left Face
	triangle->index(4); triangle->index(2); triangle->index(6);
	triangle->index(4); triangle->index(0); triangle->index(2);

	//Back Face
	triangle->index(6); triangle->index(7); triangle->index(5);
	triangle->index(6); triangle->index(5); triangle->index(4);

	triangle->end();
	return triangle;
}

void TutorialApplication::createScene(void)
{
    // Create your scene here :)
	ManualObject *triangle = createCube(0, 0, 0, 20, ColourValue::Blue);
	//ManualObject *triangle2 = createCube(30, 0, 0, 30, ColourValue::Red);
	//ManualObject *triangle3 = createCube(-50, 0, -100, 200, ColourValue::Green);


	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(triangle);
	//mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(triangle2);
	//mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(triangle3);
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
