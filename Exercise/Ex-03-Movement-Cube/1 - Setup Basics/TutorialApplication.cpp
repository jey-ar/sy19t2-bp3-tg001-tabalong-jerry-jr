/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}
//---------------------------------------------------------------------------

ManualObject* TutorialApplication::createCube(float size = 1, ColourValue color = ColourValue::White) {

	ManualObject *cube = mSceneMgr->createManualObject();

	cube->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_LIST);

	cube->colour(color);

	//Front
	cube->position(-(size / 2), -(size / 2), (size / 2)); //BL 0
	cube->position((size / 2), -(size / 2), (size / 2)); //BR 1
	cube->position(-(size / 2), (size / 2), (size / 2)); //TL 2
	cube->position((size / 2), (size / 2), (size / 2)); //TR 3

	cube->colour(ColourValue::Red);
	//Back
	cube->position(-(size / 2), -(size / 2), -(size / 2)); //BL 4
	cube->position((size / 2), -(size / 2), -(size / 2)); //BR 5
	cube->position(-(size / 2), (size / 2), -(size / 2)); //TL 6
	cube->position((size / 2), (size / 2), -(size / 2)); //TR 7

	//Front Face
	cube->index(0); cube->index(3); cube->index(2);
	cube->index(0); cube->index(1); cube->index(3);

	//Right Face
	cube->index(1); cube->index(7); cube->index(3);
	cube->index(1); cube->index(5); cube->index(7);

	//Top Face
	cube->index(2); cube->index(7); cube->index(6);
	cube->index(2); cube->index(3); cube->index(7);

	//Bottom Face
	cube->index(4); cube->index(1); cube->index(0);
	cube->index(4); cube->index(5); cube->index(1);

	//Left Face
	cube->index(4); cube->index(2); cube->index(6);
	cube->index(4); cube->index(0); cube->index(2);

	//Back Face
	cube->index(6); cube->index(7); cube->index(5);
	cube->index(6); cube->index(5); cube->index(4);

	cube->end();
	return cube;
}

void TutorialApplication::createScene(void)
{
    // Create your scene here :)
	ManualObject *cube = createCube(20, ColourValue::Blue);

	cubeNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	cubeNode->attachObject(cube);
}

//UPDATE
bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_L)) horDir = 1;
	else if (mKeyboard->isKeyDown(OIS::KeyCode::KC_J)) horDir = -1;
	else horDir = 0;
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_I)) verDir = 1;
	else if (mKeyboard->isKeyDown(OIS::KeyCode::KC_K)) verDir = -1;
	else verDir = 0;

	if ((horDir != 0) || (verDir != 0)) speed += 0.5f;
	else speed = 0;
	cubeNode->translate(speed * evt.timeSinceLastFrame * horDir, speed * evt.timeSinceLastFrame * verDir, 0);

	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD8)) xRot = -1;
	else if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD2)) xRot = 1;
	else xRot = 0;
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD4)) yRot = -1;
	else if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD6)) yRot = 1;
	else yRot = 0;

	cubeNode->rotate(Vector3(1, 0, 0), Radian(Degree(xRot * 30 * evt.timeSinceLastFrame)));
	cubeNode->rotate(Vector3(0, 1, 0), Radian(Degree(yRot * 30 * evt.timeSinceLastFrame)));

	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
